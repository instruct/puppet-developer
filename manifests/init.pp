class puppet_developer (
  String $user,
  String $full_name,
) {

  package {['vim-enhanced', 'gitk', 'xorg-x11-xauth',
            'xorg-x11-fonts-misc', 'xorg-x11-fonts-100dpi',
            'xorg-x11-fonts-Type1']:
    ensure => installed,
  }

  package {['puppet-lint', 'rspec-puppet', 'puppetlabs_spec_helper']:
    ensure   => installed,
    provider => 'puppet_gem',
  }

  file_line {'ssh_auth_sock':
    path => '/etc/sudoers',
    line => 'Defaults    env_keep += "SSH_AUTH_SOCK"',
  }

  file { '/root/.gitconfig':
    ensure  => file,
    content => template('puppet_developer/gitconfig.erb'),
  }

  file { '/root/.vimrc':
    ensure => file,
    source => 'puppet:///modules/puppet_developer/vimrc',
  }

  file {'/root/.puppet-git-hooks':
    ensure  => directory,
    source  => 'puppet:///modules/puppet_developer/puppet-git-hooks',
    recurse => true,
    notify  => [ Exec['chmod'], Exec['chmod-hooks'] ],
  }

  exec {'chmod':
    command     => 'chmod +x /root/.puppet-git-hooks/{deploy-git-hook,post-update,pre-commit,pre-receive,update}',
    refreshonly => true,
    path        => '/usr/bin'
  }

  exec {'chmod-hooks':
    command     => 'chmod +x /root/.puppet-git-hooks/commit_hooks/*.sh',
    refreshonly => true,
    path        => '/usr/bin'
  }

  file {'/root/.vim':
    ensure  => directory,
    mode    => '0644',
    source  => 'puppet:///modules/puppet_developer/vim',
    recurse => true,
    purge   => true,
    force   => true,
  }

  file { '/root/.bashrc':
    ensure => file,
    source => 'puppet:///modules/puppet_developer/bashrc',
  }

  file { '/root/.bash_profile':
    ensure => file,
    source => 'puppet:///modules/puppet_developer/bash_profile',
  }

  file { '/root/.bashrc.puppet':
    ensure  => file,
    content => template('puppet_developer/bashrc.puppet.erb'),
  }

  file { '/usr/local/bin/start_development.sh':
    ensure => file,
    mode   => '0755',
    source => 'puppet:///modules/puppet_developer/start_development.sh',
  }

}
