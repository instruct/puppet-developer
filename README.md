# Puppet Developer

#### Tabela de conteudo

1. [Sobre](#sobre)
2. [Autores](#autores)
3. [Contribuidores](#contribuidores)
4. [Funcionamento](#funcinamento)
5. [Requisitos](#requisitos)
6. [Uso](#setup)

## Sobre

Este módulo contém o resource type `puppet_developer`, que configura a conta de um usuário para desenvolvimento no padrão da Instruct e Puppet Labs.

Este módulo é baseado em configurações da Puppet Labs.

## Autor

* Miguel Di Ciurcio Filho (miguel@instruct.com.br)
* Guto Carvalho (gutocarvalho@instruct.com.br)

## Contribuidores

Faça parte da lista de contribuidores, envie seu PR.

## Funcionamento

### vim

Este módulo instala os pacotes e dependências do editor vim via yum.

* vim-enhanced
* gitk
* xorg-x11-xauth
* xorg-x11-fonts-misc
* xorg-x11-fonts-100dpi
* xorg-x11-fonts-Type1

Ele instala diversos plugins para trabalhar com código puppet.

Ele configura o vim de acordo com o puppet style guide para edição de manifests.

Ele também configura o vimrc global e o .vimrc do usuário root.

### puppet

Este módulo instala os pacotes para testes de código puppet via gem.

* puppet-lint
* rspec-puppet
* puppetlabs_spec_helper

### git

Este módulo instala diversos hooks git

* checagem obrigatória do puppet-parser antes do commit
* checagem obrigatória do puppet-lint antes do commit
* checagem obrigatória de arquivos yaml
* checagem obrigatória de tamples epp
* checagem obrigatória de templates erb
* checagem obrigatória da sintaxe do r10k
* checagem obrigatória do puppet-rspec se existir testes para as classes

Este módulo configura o .gitconfig do usuário root

### bash

Este módulo implementa diversas configurações de bash para facilitar o desenvolvimento

* Variavel PS1 com informacoes do GIT e BRANCH

## Requisitos

Você precisa ter um repositorio control-repo de produção com suas chaves devidamente autorizadas. Tome de exemplo o repositorio da instruct

    https://bitbucket.org/instruct/puppet-controlrepo

Ou se preferir use o exemplo da Puppet Labs

    https://github.com/puppetlabs/control-repo

## Uso

Declare a classe principal conforme abaixo e aplique em seu node de desenvolvimento

```puppet
  class { 'puppet_developer':
    key      => 'AAASDASDAS....',
    key_type => 'ssh-dsa'
  }
```

Se preferir chame-o diretamente do console

    puppet apply -e "class {'puppet_developer': user => 'nome', full_name => 'Nome Completo'}"

Para iniciar o desenvolvimento edite o arquivo start_development.sh e ajuste o endereço do repo git que você usa para o seu r10k/control-repo

    vim /usr/local/bin/start_development.sh

E chame o comando para começar a desenvolver

     /usr/local/bin/start_development.sh modulo-xpto

Com isso o script vai criar todo a estrutura em

     /etc/puppetlabs/code/environments/modulo-xpto

A partir daí basta desenvolver normalmente e empurrar as mudanças dessa branch pro seu contro-repo.