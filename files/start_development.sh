#!/bin/bash

BRANCH=$1
git clone -b production https://bitbucket.org/instruct/puppet-controlrepo.git /etc/puppetlabs/code/environments/$BRANCH

if [ $? -ne 0 ]; then
  exit 1
fi

cd /etc/puppetlabs/code/environments/$BRANCH
git checkout -b $BRANCH
r10k puppetfile install -v debug

MODULOS=$(egrep -o '([a-z_])+(\.git)' /etc/puppetlabs/code/environments/$BRANCH/Puppetfile | sed -e 's/.git//')

cd ~/.puppet-git-hooks
for i in $MODULOS; do
    bash ./deploy-git-hook -d /etc/puppetlabs/code/environments/$BRANCH/modules/$i -c;
done

bash ./deploy-git-hook -d /etc/puppetlabs/code/environments/$BRANCH -c
